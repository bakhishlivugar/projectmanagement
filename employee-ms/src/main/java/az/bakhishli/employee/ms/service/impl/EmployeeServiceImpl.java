package az.bakhishli.employee.ms.service.impl;

import az.bakhishli.employee.ms.domain.Employee;
import az.bakhishli.employee.ms.dto.EmployeeRequestDto;
import az.bakhishli.employee.ms.dto.EmployeeResponseDto;
import az.bakhishli.employee.ms.repository.EmployeeRepository;
import az.bakhishli.employee.ms.service.EmployeeService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class EmployeeServiceImpl implements EmployeeService {

    private final EmployeeRepository empRepository;
    private final ModelMapper mapper;

    @Override
    public EmployeeResponseDto getById(Long id) {
        Employee employee = empRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Employee not found with given id"));
        return mapper.map(employee, EmployeeResponseDto.class);
    }

    @Override
    public void create(EmployeeRequestDto dto) {
        Employee employee = Employee.builder()
                .firstName(dto.getFirstName())
                .lastName(dto.getLastName())
                .email(dto.getEmail())
                .build();
        empRepository.save(employee);
    }

    @Override
    public EmployeeResponseDto update(Long id, EmployeeRequestDto dto) {
        Employee employee = empRepository.findById(id)
                .orElseThrow(() -> new RuntimeException("Employee not found with given id"));
        employee.setFirstName(dto.getFirstName());
        employee.setLastName(dto.getLastName());
        employee.setEmail(dto.getEmail());
        empRepository.save(employee);
        return mapper.map(employee, EmployeeResponseDto.class);
    }

    @Override
    public void delete(Long id) {
        empRepository.deleteById(id);
    }
}
