package az.bakhishli.employee.ms.service;

import az.bakhishli.employee.ms.dto.EmployeeRequestDto;
import az.bakhishli.employee.ms.dto.EmployeeResponseDto;

public interface EmployeeService {
    EmployeeResponseDto getById(Long id);
    void create(EmployeeRequestDto dto);
    EmployeeResponseDto update(Long id, EmployeeRequestDto dto);
    void delete(Long id);
}
