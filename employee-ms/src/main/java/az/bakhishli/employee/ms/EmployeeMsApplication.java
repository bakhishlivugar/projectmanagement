package az.bakhishli.employee.ms;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.cloud.openfeign.EnableFeignClients;

@EnableFeignClients
@SpringBootApplication
public class EmployeeMsApplication {
    public static void main(String[] args) {
        SpringApplication.run(EmployeeMsApplication.class, args);
    }
}
