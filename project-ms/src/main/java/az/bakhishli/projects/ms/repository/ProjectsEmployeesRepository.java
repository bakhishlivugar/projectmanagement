package az.bakhishli.projects.ms.repository;

import az.bakhishli.projects.ms.domain.ProjectsEmployees;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.stereotype.Repository;

@Repository
public interface ProjectsEmployeesRepository extends JpaRepository<ProjectsEmployees, Long> {

}
