package az.bakhishli.projects.ms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@AllArgsConstructor
@NoArgsConstructor
@Builder
public class AddEmployeeDto {
    private Long projectId;
    private Long employeeId;
}
