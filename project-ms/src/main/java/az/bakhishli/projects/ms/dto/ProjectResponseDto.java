package az.bakhishli.projects.ms.dto;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

@Data
@NoArgsConstructor
@Builder
@AllArgsConstructor
public class ProjectResponseDto {
    private Long id;
    private String title;
    private Long EmployeeId;
}
