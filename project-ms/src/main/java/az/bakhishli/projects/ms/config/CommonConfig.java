package az.bakhishli.projects.ms.config;

import org.springframework.context.annotation.ComponentScan;
import org.springframework.context.annotation.Configuration;

@Configuration
@ComponentScan(basePackages = {"az.bakhishli.common.config"})
public class CommonConfig {
}
