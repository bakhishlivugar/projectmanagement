package az.bakhishli.projects.ms.service;

import az.bakhishli.projects.ms.dto.AddEmployeeDto;
import az.bakhishli.projects.ms.dto.ProjectRequestDto;
import az.bakhishli.projects.ms.dto.ProjectResponseDto;

public interface ProjectService {
    ProjectResponseDto getById(Long id);
    void create(ProjectRequestDto dto);
    ProjectResponseDto update(Long id, ProjectRequestDto dto);
    void delete(Long id);
    ProjectResponseDto addStudent(AddEmployeeDto dto);
}
