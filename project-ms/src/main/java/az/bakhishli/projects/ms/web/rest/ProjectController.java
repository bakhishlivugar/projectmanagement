package az.bakhishli.projects.ms.web.rest;

import az.bakhishli.projects.ms.dto.AddEmployeeDto;
import az.bakhishli.projects.ms.dto.ProjectRequestDto;
import az.bakhishli.projects.ms.dto.ProjectResponseDto;
import az.bakhishli.projects.ms.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.DeleteMapping;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.PostMapping;
import org.springframework.web.bind.annotation.PutMapping;
import org.springframework.web.bind.annotation.RequestBody;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RestController;

@RestController
@RequiredArgsConstructor
@RequestMapping("api/projects")
public class ProjectController {

    private final ProjectService projectService;

    @GetMapping("/{id}")
    public ResponseEntity<ProjectResponseDto> getById(@PathVariable Long id){
        return ResponseEntity.ok(projectService.getById(id));
    }

    @PostMapping("/create")
    public ResponseEntity<Void> create(@RequestBody ProjectRequestDto dto){
        projectService.create(dto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }

    @PutMapping("/update/{id}")
    public ResponseEntity<ProjectResponseDto> update(@PathVariable Long id,
                                                     @RequestBody ProjectRequestDto dto){
        return ResponseEntity.ok(projectService.update(id, dto));
    }

    @DeleteMapping("/{id}")
    public ResponseEntity<Void> delete(@PathVariable Long id){
        projectService.delete(id);
        return ResponseEntity.status(HttpStatus.OK).build();
    }

    @PostMapping("/add-employee")
    public ResponseEntity<ProjectResponseDto> addEmployee(@RequestBody AddEmployeeDto dto){
        projectService.addStudent(dto);
        return ResponseEntity.status(HttpStatus.CREATED).build();
    }
}
