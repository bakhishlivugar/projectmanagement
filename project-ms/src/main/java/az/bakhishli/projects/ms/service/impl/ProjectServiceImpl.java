package az.bakhishli.projects.ms.service.impl;

import az.bakhishli.projects.ms.domain.Project;
import az.bakhishli.projects.ms.domain.ProjectsEmployees;
import az.bakhishli.projects.ms.dto.AddEmployeeDto;
import az.bakhishli.projects.ms.dto.ProjectRequestDto;
import az.bakhishli.projects.ms.dto.ProjectResponseDto;
import az.bakhishli.projects.ms.repository.ProjectRepository;
import az.bakhishli.projects.ms.repository.ProjectsEmployeesRepository;
import az.bakhishli.projects.ms.service.ProjectService;
import lombok.RequiredArgsConstructor;
import org.modelmapper.ModelMapper;
import org.springframework.stereotype.Service;

@Service
@RequiredArgsConstructor
public class ProjectServiceImpl implements ProjectService {

    private final ProjectRepository projectRepository;
    private final ProjectsEmployeesRepository projectsEmployeesRepository;
    private final ModelMapper mapper;

    @Override
    public ProjectResponseDto getById(Long id) {
        Project project = projectRepository.findById(id).
                orElseThrow(() -> new RuntimeException("The project with given id is not found"));
        return mapper.map(project, ProjectResponseDto.class);
    }

    @Override
    public void create(ProjectRequestDto dto) {
        Project project = Project.builder()
                .title(dto.getTitle())
                .build();
        projectRepository.save(project);
    }

    @Override
    public ProjectResponseDto update(Long id, ProjectRequestDto dto) {
        Project project = projectRepository.findById(id).
                orElseThrow(() -> new RuntimeException("The project with given id is not found"));
        project.setTitle(dto.getTitle());
        return mapper.map(projectRepository.save(project), ProjectResponseDto.class);
    }

    @Override
    public void delete(Long id) {
        projectRepository.deleteById(id);
    }

    @Override
    public ProjectResponseDto addStudent(AddEmployeeDto dto) {
        ProjectsEmployees projectsEmployees = ProjectsEmployees.builder()
                .employeeId(dto.getEmployeeId())
                .projectId(dto.getProjectId())
                .build();
        return mapper.map(projectsEmployeesRepository.save(projectsEmployees), ProjectResponseDto.class);
    }
}
