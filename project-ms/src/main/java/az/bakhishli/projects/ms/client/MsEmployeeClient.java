package az.bakhishli.projects.ms.client;

import az.bakhishli.common.dto.EmployeeIdContainerDto;
import org.springframework.cloud.openfeign.FeignClient;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.PathVariable;

@FeignClient(name = "${client.employee-ms.name}",
        url = "${client.employee-ms.host}${client.employee-ms.path}")
public interface MsEmployeeClient {
    @GetMapping("/employee/{id}")
    EmployeeIdContainerDto getById(@PathVariable Long id);
}
